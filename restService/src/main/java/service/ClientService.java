package service;

import java.util.List;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import model.Client;
import util.JsonFileReader;

public class ClientService {
	 
	private Map<String, Client> clientsMap;

    public ClientService() {
        clientsMap = new HashMap<>();
        loadClientsFromJsonFile();
    }

    private void loadClientsFromJsonFile() {
        try {
            List<Client> clients = JsonFileReader.readJsonToObject("clients.json");
            for (Client client : clients) {
                clientsMap.put(client.getCode(), client);
            }
        } catch (IOException e) {
            // Manejar el error apropiadamente (por ejemplo, registrar o lanzar una excepción)
            e.printStackTrace();
        }
    }

    public Client getClientByCode(String code) {
        return clientsMap.get(code);
    }
}