package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Client;
import service.ClientService;

@WebServlet("/client/*")
public class ClientServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private ClientService clientService;

    public ClientServlet() {
        super();
        clientService = new ClientService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        String pathInfo = request.getPathInfo();
        if (pathInfo != null) {
            String code = pathInfo.substring(1);
            Client client = clientService.getClientByCode(code);

            if (client != null) {
                response.setContentType("application/json");
                PrintWriter out = response.getWriter();
                out.println(client.toJsonString());
            } else {
                response.sendError(HttpServletResponse.SC_NOT_FOUND, "Client not found");
            }
        } else {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid request");
        }
    }
}