package util;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import model.Client;

public class JsonFileReader {
    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static List<Client> readJsonToObject(String filename) throws IOException {
        InputStream inputStream = JsonFileReader.class.getClassLoader().getResourceAsStream(filename);

        try {
            return objectMapper.readValue(inputStream, new TypeReference<List<Client>>() {});
        } catch (IOException e) {
            // Manejar el error apropiadamente (por ejemplo, registrar o lanzar una excepción)
            e.printStackTrace();
            return null;
        }
    }
}